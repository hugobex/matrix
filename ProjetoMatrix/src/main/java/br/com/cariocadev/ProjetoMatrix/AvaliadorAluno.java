package br.com.cariocadev.ProjetoMatrix;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AvaliadorAluno {

	public BigDecimal getMaiorNota(BigDecimal[] notas) {
		if (!validaEntrada(notas))
			throw new IllegalArgumentException();

		Collections.sort(Arrays.asList(notas), Collections.reverseOrder());
		return notas[0];
	}

	public BigDecimal getMedia(BigDecimal nota1, BigDecimal nota2, BigDecimal nota3) {
		if (!validaEntrada(nota1, nota2, nota3))
			throw new IllegalArgumentException();

		return new BigDecimal(0.0).add(nota1).add(nota2).add(nota3)
				.divide(new BigDecimal(3), 2, BigDecimal.ROUND_HALF_UP);
	}

	public String getStatus(BigDecimal nota1, BigDecimal nota2, BigDecimal nota3) {
		if (!validaEntrada(nota1, nota2, nota3))
			throw new IllegalArgumentException();

		BigDecimal media = getMedia(nota1, nota2, nota3);
		String statusAluno = "REPROVADO";

		if (media.compareTo(BigDecimal.valueOf(6.00).setScale(2, BigDecimal.ROUND_HALF_UP)) > 0) {
			return "APROVADO";

		} else if (media.compareTo(BigDecimal.valueOf(4.00).setScale(2, BigDecimal.ROUND_HALF_UP)) >= 0
				&& media.compareTo(BigDecimal.valueOf(6.00).setScale(2, BigDecimal.ROUND_HALF_UP)) < 0) {
			return "PROVA_FINAL";

		}
		
		return statusAluno;
	}

	private Boolean validaEntrada(BigDecimal... notas) {
		if (notas == null)
			return false;

		List<BigDecimal> notasOK = Stream.of(notas)
				.filter(n -> n != null && n.compareTo(BigDecimal.ZERO) > 0
						&& n.compareTo(BigDecimal.valueOf(10.00).setScale(2, BigDecimal.ROUND_HALF_UP)) <= 0)
				.collect(Collectors.toList());

		return notasOK.size() == notas.length;
	}

}