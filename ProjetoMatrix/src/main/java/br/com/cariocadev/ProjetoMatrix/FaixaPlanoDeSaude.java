package br.com.cariocadev.ProjetoMatrix;

import java.math.BigDecimal;

public class FaixaPlanoDeSaude {

	private BigDecimal valor;
	private Integer idadeMinima;
	private Integer idadeMaxima;
	
	public FaixaPlanoDeSaude(BigDecimal valor, Integer idadeMinima, Integer idadeMaxima) {
		super();
		this.valor = valor;
		this.idadeMinima = idadeMinima;
		this.idadeMaxima = idadeMaxima;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getIdadeMinima() {
		return idadeMinima;
	}

	public void setIdadeMinima(Integer idadeMinima) {
		this.idadeMinima = idadeMinima;
	}

	public Integer getIdadeMaxima() {
		return idadeMaxima;
	}

	public void setIdadeMaxima(Integer idadeMaxima) {
		this.idadeMaxima = idadeMaxima;
	}

}
