package br.com.cariocadev.ProjetoMatrix;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class CalculadoraSalario {
	
	private List<FaixaPlanoDeSaude> faixasDePlanos = new ArrayList<>();;

	
	public CalculadoraSalario() {
		iniciaTabelaPlanoDeSaude();
	}

	public BigDecimal getSalarioLiquido(BigDecimal salarioBruto, BigDecimal percentualImpostoINSS) {
		if(!validarEntrada(salarioBruto))
			throw new IllegalArgumentException();
		
		BigDecimal salarioLiquido = new BigDecimal(salarioBruto.toString()).setScale(2, BigDecimal.ROUND_HALF_UP);		
		return salarioLiquido.subtract(salarioBruto.multiply(percentualImpostoINSS).divide(BigDecimal.valueOf(100)));
	}

	public BigDecimal getValorINSS(BigDecimal salarioBruto) {
		
		if(!validarEntrada(salarioBruto))
			throw new IllegalArgumentException();	
		
		BigDecimal desconto = BigDecimal.ZERO;
		if (salarioBruto.compareTo(BigDecimal.ZERO) > 0 && salarioBruto.compareTo(BigDecimal.valueOf(1693.72)) <= 0) {
			desconto = getDesconto(salarioBruto, BigDecimal.valueOf(8.0));
			
		} else if (salarioBruto.compareTo(BigDecimal.valueOf(1693.73).setScale(2, BigDecimal.ROUND_HALF_UP)) >= 0 && salarioBruto.compareTo(BigDecimal.valueOf(2822.90)) <= 0) {
			desconto = getDesconto(salarioBruto, BigDecimal.valueOf(9.0));

		} else if (salarioBruto.compareTo(BigDecimal.valueOf(2822.90).setScale(2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP)) > 0) {
			desconto = getDesconto(salarioBruto, BigDecimal.valueOf(11.0));
		}
			
	  return desconto;

	}
	
	private BigDecimal getDesconto(BigDecimal value, BigDecimal percentual) {
		return value.multiply(percentual).divide(BigDecimal.valueOf(100.00)).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal getValorPlanoDeSaude(Integer idade) {
		if (idade == null || idade < 0)
			throw new IllegalArgumentException();

		Optional<FaixaPlanoDeSaude> faixa = 
				faixasDePlanos
					.stream()
					.filter(f -> f.getIdadeMinima() <= idade && f.getIdadeMaxima() >= idade)
					.findFirst();

		return faixa.isPresent() && faixa.get().getValor() != null
				? faixa.get().getValor().setScale(2, RoundingMode.HALF_UP) 
				: new BigDecimal(0);
	}
	

	public Boolean validarEntrada(BigDecimal value) {
		return value != null && value.doubleValue() > 0.00;
	}
	
	public void iniciaTabelaPlanoDeSaude() {
		
		faixasDePlanos = Arrays.asList(
					new FaixaPlanoDeSaude(BigDecimal.valueOf(75.00).setScale(2,   RoundingMode.HALF_UP), 0, 9),
					new FaixaPlanoDeSaude(BigDecimal.valueOf(112.50).setScale(2,  RoundingMode.HALF_UP), 10, 19),
					new FaixaPlanoDeSaude(BigDecimal.valueOf(168.75).setScale(2,  RoundingMode.HALF_UP), 20, 29),
					new FaixaPlanoDeSaude(BigDecimal.valueOf(253.13).setScale(2,  RoundingMode.HALF_UP), 30, 39),
					new FaixaPlanoDeSaude(BigDecimal.valueOf(379.69).setScale(2,  RoundingMode.HALF_UP), 40, 49),
					new FaixaPlanoDeSaude(BigDecimal.valueOf(569.54).setScale(2,  RoundingMode.HALF_UP), 50, 59),
					new FaixaPlanoDeSaude(BigDecimal.valueOf(854.30).setScale(2,  RoundingMode.HALF_UP), 60, 200)
				);
	}
}