package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Curso;

public class CursoService {
	
	private Map<String, Curso> hashCurso = new HashMap<>();

	public Curso cadastrar(Curso curso) {

		if (ObjectUtils.isEmpty(curso))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(curso.getCodigo()) && hashCurso.containsKey(curso.getCodigo()))
			return atualizar(curso);

		curso.setCodigo(gerarCodigo(curso));
		hashCurso.put(curso.getCodigo(), curso);
		return curso;
	}

	public Curso atualizar(Curso curso) {

		if (ObjectUtils.isEmpty(curso) || !hashCurso.containsKey(curso.getCodigo()))
			throw new IllegalArgumentException("Parametro inválido");

		hashCurso.put(curso.getCodigo(), curso);
		return curso;

	}

	public void remover(Curso curso) {
		if (ObjectUtils.isEmpty(curso))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashCurso.remove(curso.getCodigo());
	}

	public Curso recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashCurso.get(codigo);
	}
	
	private String gerarCodigo(Curso curso) {

		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashCurso.size() + 1);
			
		return matricula.toString();
	}
}
