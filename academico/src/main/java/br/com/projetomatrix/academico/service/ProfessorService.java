package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Professor;
import br.com.projetomatrix.academico.model.Turma;

public class ProfessorService {

	private Map<String, Professor> hashProfessor = new HashMap<>();
	private TurmaService turmaService = new TurmaService();

	public List<Turma> recuperarTurmas(Professor professor){

		if (ObjectUtils.isEmpty(professor))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");
	
		return turmaService.buscarTurmasProfessor(professor);
	}

	public Professor cadastrar(Professor professor) {

		if (ObjectUtils.isEmpty(professor))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(professor.getMatricula()) && hashProfessor.containsKey(professor.getMatricula()))
			return atualizar(professor);

		professor.setMatricula(gerarMatricula(professor));
		hashProfessor.put(professor.getMatricula(), professor);
		return professor;
	}

	public Professor atualizar(Professor professor) {

		if (ObjectUtils.isEmpty(professor) || !hashProfessor.containsKey(professor.getMatricula()))
			throw new IllegalArgumentException("Parametro inválido");

		hashProfessor.put(professor.getMatricula(), professor);
		return professor;

	}

	public void remover(Professor professor) {
		if (ObjectUtils.isEmpty(professor))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashProfessor.remove(professor.getMatricula());
	}

	public Professor recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashProfessor.get(codigo);
	}
	
	private String gerarMatricula(Professor professor) {
		
		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear())
			.append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashProfessor.size() + 1);
					
		return matricula.toString();
	}
}
