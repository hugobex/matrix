package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Aluno;
import br.com.projetomatrix.academico.model.Avaliacao;
import br.com.projetomatrix.academico.model.Turma;

public class AvaliacaoService {

	private Map<String, Avaliacao> hashAvaliacao = new HashMap<>();

	public Avaliacao cadastrar(Avaliacao avaliacao) {

		if (ObjectUtils.isEmpty(avaliacao))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(avaliacao.getCodigo()) && hashAvaliacao.containsKey(avaliacao.getCodigo()))
			return atualizar(avaliacao);

		avaliacao.setCodigo(gerarCodigo(avaliacao));
		hashAvaliacao.put(avaliacao.getCodigo(), avaliacao);
		return avaliacao;
	}

	public Avaliacao atualizar(Avaliacao avaliacao) {

		if (ObjectUtils.isEmpty(avaliacao) || !hashAvaliacao.containsKey(avaliacao.getCodigo()))
			throw new IllegalArgumentException("Parametro inválido");

		hashAvaliacao.put(avaliacao.getCodigo(), avaliacao);
		return avaliacao;

	}

	public void remover(Avaliacao avaliacao) {
		if (ObjectUtils.isEmpty(avaliacao))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashAvaliacao.remove(avaliacao.getCodigo());
	}

	public Avaliacao recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashAvaliacao.get(codigo);
	}
	
	private String gerarCodigo(Avaliacao avaliacao) {

		StringBuffer codigo = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		codigo
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashAvaliacao.size() + 1);
			
		return codigo.toString();
	}

	/*
	 * Com Java 8 \o/
	 */
	public List<Avaliacao> recuperarAvaliacoesAluno(Aluno aluno, Turma turma){		
		return hashAvaliacao.values()
				.stream()
				.filter(a -> 
					a.getAluno().getMatricula().equals(aluno.getMatricula())
					&& a.getTurma().getCodigo().equals(turma.getCodigo()))				
				.collect(Collectors.toList());
	}
	
	public List<Avaliacao> recuperarTodasAvaliacoesAluno(Aluno aluno){		
		return hashAvaliacao.values()
				.stream()
				.filter(a -> a.getAluno().getMatricula().equals(aluno.getMatricula()))				
				.collect(Collectors.toList());
	}

/*
 * Sem Java 8 :/
 */
//	public List<Avaliacao> getAvaliacoes(Aluno aluno, Turma turma) {
//		ArrayList<Avaliacao> avaliacoes = new ArrayList<>(hashAvaliacao.values());
//		List<Avaliacao> avaliacoesAluno = new ArrayList<>();
//
//		for (Avaliacao avaliacao : avaliacoes) {
//			if (avaliacao.getTurma().getCodigo().equals(turma.getCodigo())
//					&& avaliacao.getAluno().getMatricula().equals(aluno.getMatricula()))
//				avaliacoesAluno.add(avaliacao);
//
//		}
//
//		return avaliacoesAluno;
//	}
	
}
