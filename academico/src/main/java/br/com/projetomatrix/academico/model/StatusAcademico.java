package br.com.projetomatrix.academico.model;

public enum StatusAcademico {

	APROVADO, REPROVADO, PROVA_FINAL
}
