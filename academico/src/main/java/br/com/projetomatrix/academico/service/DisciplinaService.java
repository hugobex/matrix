package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Disciplina;

public class DisciplinaService {

	private Map<String, Disciplina> hashCurso = new HashMap<>();

	public Disciplina cadastrar(Disciplina disciplina) {

		if (ObjectUtils.isEmpty(disciplina))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(disciplina.getCodigo()) && hashCurso.containsKey(disciplina.getCodigo()))
			return atualizar(disciplina);

		disciplina.setCodigo(gerarCodigo(disciplina));
		hashCurso.put(disciplina.getCodigo(), disciplina);
		return disciplina;
	}

	public Disciplina atualizar(Disciplina disciplina) {

		if (ObjectUtils.isEmpty(disciplina) || !hashCurso.containsKey(disciplina.getCodigo()))
			throw new IllegalArgumentException("Parametro inválido");

		hashCurso.put(disciplina.getCodigo(), disciplina);
		return disciplina;

	}

	public void remover(Disciplina disciplina) {
		if (ObjectUtils.isEmpty(disciplina))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashCurso.remove(disciplina.getCodigo());
	}

	public Disciplina recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashCurso.get(codigo);
	}
	
	private String gerarCodigo(Disciplina disciplina) {

		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashCurso.size() + 1);
			
		return matricula.toString();
	}
}
