package br.com.projetomatrix.academico.model;

public enum Tutulo {

	GRADUACAO, ESPECIALIZACAO, MESTRADO, DOUTORADO
}
