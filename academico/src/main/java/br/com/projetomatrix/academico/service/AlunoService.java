package br.com.projetomatrix.academico.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Aluno;
import br.com.projetomatrix.academico.model.Avaliacao;
import br.com.projetomatrix.academico.model.StatusAcademico;
import br.com.projetomatrix.academico.model.Turma;

public class AlunoService {

//	buscar o boletim do aluno para uma determinada disciplina
//	buscar histórico de um determinado aluno.
	
	private Map<String, Aluno> hashAluno = new HashMap<>();
	private TurmaService turmaService = new TurmaService();
	private AvaliacaoService avaliacaoService = new AvaliacaoService();
	
	
	public StatusAcademico recuperarStatusAcademico(Aluno aluno, Turma turma) {

		if (!validarAlunoMatriculado(aluno) || ObjectUtils.isEmpty(turma))
			throw new IllegalArgumentException();

		BigDecimal media = getMediaAluno(aluno, turma);

		if (media.compareTo(new BigDecimal(6.00).setScale(2, BigDecimal.ROUND_HALF_UP)) > 0) {
			return StatusAcademico.APROVADO;

		} else if (media.compareTo(new BigDecimal(4.00).setScale(2, BigDecimal.ROUND_HALF_UP)) >= 0
				&& media.compareTo(new BigDecimal(6.00).setScale(2, BigDecimal.ROUND_HALF_UP)) < 0) {
			return StatusAcademico.PROVA_FINAL;
		}
		return StatusAcademico.REPROVADO;

	}
	
	public BigDecimal getMediaAluno(Aluno aluno, Turma turma) {		
		return calcularMedia(avaliacaoService.recuperarAvaliacoesAluno(aluno, turma));		
	}
	
	/*
	 * Com Java 8 \o/
	 */
	private BigDecimal calcularMedia(List<Avaliacao> avaliacaoes) {
		if (ObjectUtils.isEmpty(avaliacaoes))
			throw new IllegalArgumentException();

		return BigDecimal.valueOf(
					avaliacaoes
						.stream()
						.mapToDouble(e -> e.getNota().doubleValue()).average().orElse(0))
				.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
/*
 * Sem Java 8 :/
	private BigDecimal calcularMedia(List<Avaliacao> avaliacaoes) {
		if (ObjectUtils.isEmpty(avaliacaoes))
			throw new IllegalArgumentException();
		
		BigDecimal media = BigDecimal.valueOf(0);
		for (Avaliacao avaliacao : avaliacaoes) {
			media.add(avaliacao.getNota());
		}
		
		return media.divide(BigDecimal.valueOf(avaliacaoes.size())).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
 */
	
	public List<Turma> recuperarTurmas(Aluno aluno) {
		
		if(!validarAlunoMatriculado(aluno))
			throw new IllegalArgumentException();
		
		return turmaService.buscarTurmasAluno(aluno);
	}
	

	public Aluno cadastrarAluno(Aluno aluno) {

		if (ObjectUtils.isEmpty(aluno))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(aluno.getMatricula()) && hashAluno.containsKey(aluno.getMatricula()))
			return atualizarCadastroAluno(aluno);

		aluno.setMatricula(gerarMatricula(aluno));
		hashAluno.put(aluno.getMatricula(), aluno);
		return aluno;
	}

	public Aluno atualizarCadastroAluno(Aluno aluno) {

		if (ObjectUtils.isEmpty(aluno) || !hashAluno.containsKey(aluno.getMatricula()))
			throw new IllegalArgumentException("Parametro aluno inválido");

		hashAluno.put(aluno.getMatricula(), aluno);
		return aluno;

	}

	public void removerAluno(Aluno aluno) {
		if (ObjectUtils.isEmpty(aluno))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashAluno.remove(aluno.getMatricula());
	}

	public Aluno recuperarAluno(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashAluno.get(codigo);
	}
	
	private String gerarMatricula(Aluno aluno) {

		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashAluno.size() + 1);
			
		return matricula.toString();
	}
	
	private Boolean validarAlunoMatriculado(Aluno aluno) {
		return !ObjectUtils.isEmpty(aluno) 
				&& !StringUtils.isEmpty(aluno.getMatricula()) 
				&& hashAluno.containsKey(aluno.getMatricula()) ;
	}
	
}
