package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Aluno;
import br.com.projetomatrix.academico.model.Professor;
import br.com.projetomatrix.academico.model.Turma;

public class TurmaService {

	private Map<String, Turma> hashTurma = new HashMap<>();

	public Turma cadastrar(Turma turma) {

		if (ObjectUtils.isEmpty(turma))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(turma.getCodigo()) && hashTurma.containsKey(turma.getCodigo()))
			return atualizar(turma);

		turma.setCodigo(gerarCodigo(turma));
		hashTurma.put(turma.getCodigo(), turma);
		return turma;
	}

	public Turma atualizar(Turma turma) {

		if (ObjectUtils.isEmpty(turma) || !hashTurma.containsKey(turma.getCodigo()))
			throw new IllegalArgumentException("Parametro inválido");

		hashTurma.put(turma.getCodigo(), turma);
		return turma;

	}

	public void remover(Turma turma) {
		if (ObjectUtils.isEmpty(turma))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashTurma.remove(turma.getCodigo());
	}

	public Turma recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashTurma.get(codigo);
	}
	
	private String gerarCodigo(Turma turma) {

		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashTurma.size() + 1);
			
		return matricula.toString();
	}


	/*
	 * Com uso do Java 8 \o/
	 */
	public List<Turma> buscarTurmasAluno(Aluno aluno) {
		
		if (ObjectUtils.isEmpty(aluno))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");
		
		return hashTurma.values()
			.stream()
			.filter(t -> {				
				Optional<Aluno> alunoOptional = t.getAlunos()
						.stream()
						.filter(a -> a.getMatricula().equals(aluno.getMatricula())).findFirst();
				
				return alunoOptional.isPresent();
			}).collect(Collectors.toList());
	}
	
	
	public List<Turma> buscarTurmasProfessor(Professor professor) {

		if (ObjectUtils.isEmpty(professor))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");
			
		return hashTurma.values()
				.stream()
				.filter(t -> t.getProfessor().getMatricula().equals(professor.getMatricula()))
				.collect(Collectors.toList());
	}

	public List<Turma> recuperarTodasAsTurmas() {
		return  new ArrayList<>(hashTurma.values());
	}

//  Exemplo sem uso do Java 8 :/
	
//	public List<Turma> buscarTurmasProfessor(Professor professor) {
//
//		if (ObjectUtils.isEmpty(professor))
//			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");
//		
//		// Todas as turmas
//		ArrayList<Turma> turmas = new ArrayList<>(hashTurma.values());
//		
//		// Apenas turmas relacionadas ao professor
//		ArrayList<Turma> turmasProfessor = new ArrayList<>();
//		
//		for (Turma turma : turmas) {
//			Professor professorTemp = turma.getProfessor();
//			if (professorTemp.getMatricula().equals(professor.getMatricula()))
//				turmasProfessor.add(turma);
//		}
//		return turmasProfessor;
//	}
//
//	public List<Turma> buscarTurmasAluno(Aluno aluno) {
//		
//		if (ObjectUtils.isEmpty(aluno))
//			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");
//		
//		ArrayList<Turma> turmas = new ArrayList<>(hashTurma.values());
//		ArrayList<Turma> turmasAluno = new ArrayList<>();
//		for (Turma turma : turmas) {
//			List<Aluno> alunos = turma.getAlunos();
//			for (Aluno alunoTemp : alunos) {
//				if(alunoTemp.getMatricula().equals(aluno.getMatricula()))
//						turmasAluno.add(turma);
//			}
//		}
//		return turmasAluno;
//	}
		
}
