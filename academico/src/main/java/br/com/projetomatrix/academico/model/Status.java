package br.com.projetomatrix.academico.model;

public enum Status {

	ATIVO, INATIVO
}
