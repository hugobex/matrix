package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Coordenador;

public class CoordenadorService {

	private Map<String, Coordenador> hashCoordenador = new HashMap<>();

	public Coordenador cadastrar(Coordenador coordenador) {

		if (ObjectUtils.isEmpty(coordenador))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(coordenador.getMatricula()) && hashCoordenador.containsKey(coordenador.getMatricula()))
			return atualizar(coordenador);

		coordenador.setMatricula(gerarMatricula(coordenador));
		hashCoordenador.put(coordenador.getMatricula(), coordenador);
		return coordenador;
	}

	public Coordenador atualizar(Coordenador coordenador) {

		if (ObjectUtils.isEmpty(coordenador) || !hashCoordenador.containsKey(coordenador.getMatricula()))
			throw new IllegalArgumentException("Parametro inválido");

		hashCoordenador.put(coordenador.getMatricula(), coordenador);
		return coordenador;

	}

	public void remover(Coordenador coordenador) {
		if (ObjectUtils.isEmpty(coordenador))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashCoordenador.remove(coordenador.getMatricula());
	}

	public Coordenador recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashCoordenador.get(codigo);
	}
	
	private String gerarMatricula(Coordenador coordenador) {

		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashCoordenador.size() + 1);
			
		return matricula.toString();
	}
}
