package br.com.projetomatrix.academico.model;

public class Professor extends Pessoa {

	private Tutulo titulo;

	public Tutulo getTitulo() {
		return titulo;
	}

	public void setTitulo(Tutulo titulo) {
		this.titulo = titulo;
	}

}
