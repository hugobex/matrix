package br.com.projetomatrix.academico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.projetomatrix.academico.model.Aluno;
import br.com.projetomatrix.academico.service.SistemaAcademicoService;

@SpringBootApplication
public class SistemaAcademicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaAcademicoApplication.class, args);
		
		carregarDadosIniciais();
	}

	private static void carregarDadosIniciais() {

		// Exemplo
		SistemaAcademicoService sistemaAcademicoService = new SistemaAcademicoService();
		Aluno aluno = new Aluno();
		aluno.setEndereco("Rua 1");
		aluno.setNome("John Nobody");
		aluno.setTelefone("22223333");
		
		sistemaAcademicoService.cadastrarAluno(aluno);
	}
}
