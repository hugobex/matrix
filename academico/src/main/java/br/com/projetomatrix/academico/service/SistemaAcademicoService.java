package br.com.projetomatrix.academico.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.ObjectUtils;

import br.com.projetomatrix.academico.model.Aluno;
import br.com.projetomatrix.academico.model.Avaliacao;
import br.com.projetomatrix.academico.model.Boletim;
import br.com.projetomatrix.academico.model.Coordenador;
import br.com.projetomatrix.academico.model.Curso;
import br.com.projetomatrix.academico.model.Disciplina;
import br.com.projetomatrix.academico.model.Historico;
import br.com.projetomatrix.academico.model.Horario;
import br.com.projetomatrix.academico.model.Professor;
import br.com.projetomatrix.academico.model.Status;
import br.com.projetomatrix.academico.model.Turma;

public class SistemaAcademicoService {

	private AvaliacaoService avaliacaoService = new AvaliacaoService();;
	private AlunoService alunoService = new AlunoService();
	private ProfessorService professorService = new ProfessorService();
	private TurmaService turmaService = new TurmaService();
	private HorarioService horarioService = new HorarioService();
	private CursoService cursoService = new CursoService();
	private CoordenadorService coordenadorService = new CoordenadorService();
	private DisciplinaService disciplinaService = new DisciplinaService();
	
	public Boletim gerarBoletim(Aluno aluno, Turma turma) {		
		List<Avaliacao> avaliacoesAluno = avaliacaoService.recuperarAvaliacoesAluno(aluno, turma);		
		return new Boletim(aluno, turma, avaliacoesAluno);
	}
	
	
	public List<Turma> buscarTurmasProfessor(Professor professor){
		return professorService.recuperarTurmas(professor);
	}

	/*
	 * Com o uso do Java 8 \o/
	 */
	public Historico gerarHistorico(Aluno aluno) {
		
		List<Turma> turmas = alunoService.recuperarTurmas(aluno);
		List<Boletim> boletins = turmas.stream()
			.map(t -> {				
					List<Avaliacao> avaliacoes = avaliacaoService.recuperarAvaliacoesAluno(aluno, t);
					return new Boletim(aluno, t, avaliacoes);
				})
			.collect(Collectors.toList());

		return new Historico(boletins, aluno);
	}
	
/*
 * Sem o uso do Java 8 :/	
	public Historico gerarHistorico(Aluno aluno) {
		
		List<Turma> turmas = alunoService.recuperarTurmas(aluno);
		List<Boletim> boletins =  new ArrayList<>();
		for (Turma turma : turmas) {
			List<Avaliacao> avaliacoes = avaliacaoService.recuperarAvaliacoesAluno(aluno, turma);
			if(ObjectUtils.isEmpty(avaliacoes))
				continue;
			
			boletins.add(new Boletim(aluno, turma, avaliacoes));
		}
		
		return new Historico(boletins, aluno);
	}
*/
	
	public Turma matricularAlunoEmTurma(Aluno aluno, Turma turma) {
		
		if(ObjectUtils.isEmpty(aluno) || aluno.getStatus() == Status.INATIVO)
			throw new IllegalArgumentException();
			
		List<Turma> todasAsTurmas = turmaService.recuperarTodasAsTurmas();
		List<Turma> turmasLivres = todasAsTurmas
			.stream()
			.filter(t -> {
				return Collections.disjoint(t.getHorarios(), turma.getHorarios()) && !t.getAlunos().contains(aluno);
			})			
			.collect(Collectors.toList());	
		
		if(todasAsTurmas.size() != turmasLivres.size())
			throw new IllegalArgumentException("Os horarios são conflitantes");

		
		Turma turmaEmMemoria = turmaService.recuperar(turma.getCodigo());
		turmaEmMemoria.getAlunos().add(aluno);
		turmaService.atualizar(turmaEmMemoria);		
		return turmaEmMemoria;
	}

	public Turma cadastrarProfessorEmTurma(Professor professor, Turma turma) {
		// TODO
		return null;
	}

	public Aluno cadastrarAluno(Aluno aluno) {
		return alunoService.cadastrarAluno(aluno);
	}

	public Aluno atualizarAluno(Aluno aluno) {
		return alunoService.atualizarCadastroAluno(aluno);
	}

	public void removerAluno(Aluno aluno) {
		alunoService.removerAluno(aluno);
	}

	public Professor cadastrarProfessor(Professor professor) {
		return professorService.cadastrar(professor);
	}

	public Professor atualizarProfessor(Professor professor) {
		return professorService.atualizar(professor);
	}

	public void removerProfessor(Professor professor) {
		professorService.remover(professor);
	}

	public Turma cadastrarTurma(Turma turma) {
		return turmaService.cadastrar(turma);
	}

	public Turma atualizarTurma(Turma turma) {
		return turmaService.atualizar(turma);
	}

	public void removerTurma(Turma turma) {
		turmaService.remover(turma);
	}

	public Disciplina cadastrarDisciplina(Disciplina disciplina) {
		return disciplinaService.cadastrar(disciplina);
	}

	public Disciplina atualizarDisciplina(Disciplina disciplina) {
		return disciplinaService.atualizar(disciplina);
	}

	public void removerDisciplina(Disciplina disciplina) {
		disciplinaService.remover(disciplina);
	}

	public Coordenador cadastrarCoordenador(Coordenador coordenador) {
		return coordenadorService.cadastrar(coordenador);
	}

	public Coordenador atualizarCoordenador(Coordenador coordenador) {
		return coordenadorService.atualizar(coordenador);
	}

	public void removerCoordenador(Coordenador coordenador) {
		coordenadorService.remover(coordenador);
	}

	public Curso cadastrarCurso(Curso curso) {
		return cursoService.cadastrar(curso);
	}

	public Curso atualizarCurso(Curso curso) {
		return cursoService.atualizar(curso);
	}

	public void removerCurso(Curso curso) {
		cursoService.remover(curso);
	}

	public Horario cadastrarHorario(Horario horario) {
		return horarioService.cadastrar(horario);
	}

	public Horario atualizarHorario(Horario horario) {
		return horarioService.atualizar(horario);
	}

	public void removerHorario(Horario horario) {
		horarioService.remover(horario);
	}

}
