package br.com.projetomatrix.academico.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.com.projetomatrix.academico.model.Horario;

public class HorarioService {

	private Map<String, Horario> hashHorario = new HashMap<>();

	public Horario cadastrar(Horario horario) {

		if (ObjectUtils.isEmpty(horario))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		if (!StringUtils.isEmpty(horario.getCodigo()) && hashHorario.containsKey(horario.getCodigo()))
			return atualizar(horario);

		horario.setCodigo(gerarCodigo(horario));
		hashHorario.put(horario.getCodigo(), horario);
		return horario;
	}

	public Horario atualizar(Horario horario) {

		if (ObjectUtils.isEmpty(horario) || !hashHorario.containsKey(horario.getCodigo()))
			throw new IllegalArgumentException("Parametro inválido");

		hashHorario.put(horario.getCodigo(), horario);
		return horario;

	}

	public void remover(Horario horario) {
		if (ObjectUtils.isEmpty(horario))
			throw new IllegalArgumentException("Não é permitido o envio de null como argumento");

		hashHorario.remove(horario.getCodigo());
	}

	public Horario recuperar(String codigo) {
		if (StringUtils.isEmpty(codigo))
			throw new IllegalArgumentException();

		return hashHorario.get(codigo);
	}
	
	private String gerarCodigo(Horario horario) {

		StringBuffer matricula = new StringBuffer();
		LocalDateTime dataAtual = LocalDateTime.now();
		matricula
			.append(dataAtual.getYear()).append(".")
			.append(dataAtual.getMonthValue() <= 6 ? "1" : "2" )
			.append(".")
			.append(hashHorario.size() + 1);
			
		return matricula.toString();
	}
}
