package br.com.projetomatrix.lancamento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.projetomatrix.lancamento.model.FiltroData;
import br.com.projetomatrix.lancamento.model.Lancamento;
import br.com.projetomatrix.lancamento.repository.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;

	public Lancamento cadastrarLancamento(Lancamento lancamento) {

		if (ObjectUtils.isEmpty(lancamento))
			throw new IllegalArgumentException();

		return lancamentoRepository.save(lancamento);
	}

	public void removerLancamento(Long id) {
		lancamentoRepository.deleteById(id);
	}

	public Lancamento atualizarLancamento(Lancamento lancamento) {
		if(ObjectUtils.isEmpty(lancamento) || ObjectUtils.isEmpty(lancamento.getId()))
			throw new IllegalArgumentException();
		
		return lancamentoRepository.save(lancamento);
	}

	public Lancamento buscarLancamentoPorCodigo(Long id) {
		if(ObjectUtils.isEmpty(id))
			throw new IllegalArgumentException();
		
		return lancamentoRepository.findById(id).get();
	}

	public List<Lancamento> buscarTodasLancamentos(FiltroData filtroData) {
		
		return lancamentoRepository.findByVencimentoBetween(filtroData.getInicio(), filtroData.getFim()).get();
	}

}
