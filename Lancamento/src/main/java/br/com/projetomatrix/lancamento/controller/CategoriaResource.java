package br.com.projetomatrix.lancamento.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.projetomatrix.lancamento.model.Categoria;
import br.com.projetomatrix.lancamento.service.CategoriaService;

@RestController
public class CategoriaResource {

	@Autowired
	private CategoriaService categoriaService;
	
	@PutMapping("/categorias")
	public ResponseEntity<Categoria> cadastrarCategoria(@RequestBody @Valid Categoria categoria) {
		
		return new ResponseEntity<>(categoriaService.cadastrarCategoria(categoria), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/categorias/{id}")
	public void removerCategoria(@PathVariable Long id) {
		categoriaService.removerCategoria(id);
	}
	
	@PostMapping("/categorias")
	public ResponseEntity<Categoria> atualizarCategoria(@RequestBody Categoria categoria) {
		return new ResponseEntity<>(categoriaService.atualizarCategoria(categoria), HttpStatus.OK);
	}
	
	@GetMapping("/categorias/{id}")
	public ResponseEntity<Categoria> getVategoria(@PathVariable Long id) {
		return new ResponseEntity<>(categoriaService.buscarCategoriaPorCodigo(id), HttpStatus.OK);
	}
	
	@GetMapping("/categorias")
	public ResponseEntity<List<Categoria>> getCategorias() {
		return new ResponseEntity<>(categoriaService.buscarTodasCategorias(), HttpStatus.OK);
	}
}
