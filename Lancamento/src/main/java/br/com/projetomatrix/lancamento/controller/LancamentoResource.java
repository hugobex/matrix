package br.com.projetomatrix.lancamento.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.projetomatrix.lancamento.model.FiltroData;
import br.com.projetomatrix.lancamento.model.Lancamento;
import br.com.projetomatrix.lancamento.service.LancamentoService;


@RestController
public class LancamentoResource {

	@Autowired
	private LancamentoService lancamentoService;
	
	@PutMapping("/lancamentos")
	public ResponseEntity<Lancamento> cadastrarLancamento(@RequestBody @Valid Lancamento lancamento) {
		
		return new ResponseEntity<>(lancamentoService.cadastrarLancamento(lancamento), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/lancamentos/{id}")
	public void removerLancamento(@PathVariable Long id) {
		lancamentoService.removerLancamento(id);
	}
	
	@PostMapping("/lancamentos")
	public ResponseEntity<Lancamento> atualizarLancamento(@RequestBody @Valid Lancamento lancamento) {
		return new ResponseEntity<>(lancamentoService.atualizarLancamento(lancamento), HttpStatus.OK);
	}
	
	@GetMapping("/lancamentos/{id}")
	public ResponseEntity<Lancamento> recuperarLancamentoPorCodigo(@PathVariable Long id) {
		return new ResponseEntity<>(lancamentoService.buscarLancamentoPorCodigo(id), HttpStatus.OK);
	}
	
	@GetMapping("/lancamentos")
	public ResponseEntity<List<Lancamento>> recuperarLancamentos(@RequestParam("inicio") @DateTimeFormat(pattern = "dd-MM-yyyy") Date inicio, 
			@RequestParam("fim") @DateTimeFormat(pattern = "dd-MM-yyyy") Date fim ) {
		return new ResponseEntity<>(lancamentoService.buscarTodasLancamentos(new FiltroData(inicio, fim)), HttpStatus.OK);
	}
}
