package br.com.projetomatrix.lancamento.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.projetomatrix.lancamento.model.Empresa;
import br.com.projetomatrix.lancamento.service.EmpresaService;

@RestController
public class EmpresaResource {

	@Autowired
	private EmpresaService empresaService;
	
	@PutMapping("/empresas")
	public ResponseEntity<Empresa> cadastrarEmpresa(@RequestBody @Valid Empresa empresa) {
		
		return new ResponseEntity<>(empresaService.cadastrarEmpresa(empresa), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/empresas/{id}")
	public void removerEmpresa(@PathVariable Long id) {
		empresaService.removerEmpresa(id);
	}
	
	@PostMapping("/empresas")
	public ResponseEntity<Empresa> atualizarEmpresa(@RequestBody Empresa empresa) {
		return new ResponseEntity<>(empresaService.atualizarEmpresa(empresa), HttpStatus.OK);
	}
	
	@GetMapping("/empresas/{id}")
	public ResponseEntity<Empresa> getEmpresa(@PathVariable Long id) {
		return new ResponseEntity<>(empresaService.buscarEmpresaPorCodigo(id), HttpStatus.OK);
	}
	
	@GetMapping("/empresas")
	public ResponseEntity<List<Empresa>> getEmpresas() {
		return new ResponseEntity<>(empresaService.buscarTodasEmpresas(), HttpStatus.OK);
	}
}
