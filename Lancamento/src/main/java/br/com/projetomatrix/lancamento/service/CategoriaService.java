package br.com.projetomatrix.lancamento.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.projetomatrix.lancamento.model.Categoria;
import br.com.projetomatrix.lancamento.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;

	public Categoria cadastrarCategoria(@Valid Categoria categoria) {
		if (ObjectUtils.isEmpty(categoria))
			throw new IllegalArgumentException();

		return categoriaRepository.save(categoria);
	}

	public void removerCategoria(Long id) {
		categoriaRepository.deleteById(id);

	}

	public Categoria atualizarCategoria(Categoria categoria) {
		if (ObjectUtils.isEmpty(categoria) || ObjectUtils.isEmpty(categoria.getId()))
			throw new IllegalArgumentException();

		return categoriaRepository.save(categoria);
	}

	public Categoria buscarCategoriaPorCodigo(Long id) {
		if (ObjectUtils.isEmpty(id))
			throw new IllegalArgumentException();
		
		return categoriaRepository.findById(id).get();
	}

	public List<Categoria> buscarTodasCategorias() {		
		return categoriaRepository.findAll();
	}

}
