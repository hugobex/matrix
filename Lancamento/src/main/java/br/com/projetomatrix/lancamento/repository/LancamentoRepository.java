package br.com.projetomatrix.lancamento.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.projetomatrix.lancamento.model.Lancamento;

@Repository
public interface LancamentoRepository extends JpaRepository<Lancamento, Long> {

	@Query("from Lancamento e where e.vencimento >= :inicio and e.vencimento <= :fim")
	Optional<List<Lancamento>> findByVencimentoBetween(@Param("inicio") Date inicio, @Param("fim") Date fim);

}
