package br.com.projetomatrix.lancamento.model;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

public class FiltroData {

	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotEmpty
	private Date inicio;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotEmpty
	private Date fim;

	public FiltroData(Date inicio, Date fim) {
		this.inicio = inicio;
		this.fim = fim;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

}
