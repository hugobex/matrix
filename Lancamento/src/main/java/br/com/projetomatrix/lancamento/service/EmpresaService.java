package br.com.projetomatrix.lancamento.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.projetomatrix.lancamento.model.Empresa;
import br.com.projetomatrix.lancamento.repository.EmpresaRepository;

@Service
public class EmpresaService {

	@Autowired
	private EmpresaRepository empresaRepository;

	public Empresa cadastrarEmpresa(@Valid Empresa empresa) {

		if (ObjectUtils.isEmpty(empresa))
			throw new IllegalArgumentException();

		return empresaRepository.save(empresa);
	}

	public void removerEmpresa(Long id) {
		empresaRepository.deleteById(id);
	}

	public Empresa atualizarEmpresa(Empresa empresa) {
		if (ObjectUtils.isEmpty(empresa) || ObjectUtils.isEmpty(empresa.getId()))
			throw new IllegalArgumentException();

		return empresaRepository.save(empresa);
	}

	public Empresa buscarEmpresaPorCodigo(Long id) {
		if (ObjectUtils.isEmpty(id))
			throw new IllegalArgumentException();

		return empresaRepository.findById(id).get();
	}

	public List<Empresa> buscarTodasEmpresas() {
		return empresaRepository.findAll();
	}

}
